module RaceBet
  class Race

    class << self
      def score(guesses, winners)
        points = [15, 10, 5, 3, 1]
        res = 0

        guesses.each_index do |guess_key|
          if winners[guess_key] == guesses[guess_key]
            res += points[guess_key]
          elsif winners.include?(guesses[guess_key])
            res += 1
          end
        end

        res
      end
    end

  end
end
